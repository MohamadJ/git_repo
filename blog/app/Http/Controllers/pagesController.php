<?php

namespace App\Http\Controllers;

class pagesController extends Controller{

	/*process variable data
	talk to the model		
	recieve data back from the model
	compile/process data again if needed
	pass the data to the correct view*/

	public function getIndex(){
		
		return view('pages.welcome');
	}

	public function getAbout(){
		$first = "Eslam";
		$last = "Al-Jaafeel";
		$fullname = $first." ". $last;
		$email="eslam.jaafil@gmail.com";
		$data=[];
		$data['email']=$email;
		$data['fullname']=$fullname;
		return view('pages.about')->withData($data);
	}

	public function getContact(){
		return view('pages.contact');
	}

	public function postContact(){

	}
}
