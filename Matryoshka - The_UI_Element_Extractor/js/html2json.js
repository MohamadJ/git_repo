(function(global) {
	const DEBUG = false;
	var debug = DEBUG ? console.log.bind(console) : function() {};

	if (typeof module === 'object' && typeof module.exports === 'object') {
		require('htmlparser.js');
	}

	// Pass the CheckBox class name to the function
	global.getCheckedBoxes = function getCheckedBoxes(ClassName) {
		var checkboxes = document.getElementsByClassName(ClassName);
		var checkboxesChecked = [];
		// loop over them all
		for (var i = 0; i < checkboxes.length; i++) {
			// And stick the checked ones onto an array...
			if (checkboxes[i].checked) {
				checkboxesChecked.push(checkboxes[i]);
			}
		}
		// Return the array if it is non-empty, or null
		return checkboxesChecked.length > 0 ? checkboxesChecked : null;
	}

	// Call as
	/*var checkedBoxes = getCheckedBoxes("checkedBoxes");*/

	function q(v) {
		return '"' + v + '"';
	}

	var hasTag = function(tagName) {
		var i = null;
		for (i = 0; tags.length > i; i += 1) {
			if (tags[i].tagName === tagName) {
				return true;
			}
		}

		return false;
	};

	function removeDOCTYPE(html) {
		return html
			.replace(/<!doctype.*\>\n/, '')
			.replace(/<!DOCTYPE.*\>\n/, '')
	}

	function removeUnwantedProperties(html) {
		return html
			.replace(/(?:\r\n|\r|\n)/g, '')
			/*			.replace(/style[^>]*?/gi, '')*/
			.replace(/(?:\r\t|\r|\t)/g, '');
	}

	function removeScripts(html) {
		return html.replace(/<script[^>]*>.*?<\/script>/gi, '');
	}
	global.html2json = function html2json(html) {
		html = removeDOCTYPE(html);
		html = removeUnwantedProperties(html);
		html = removeScripts(html);
		var bufArray = [];
		var results = {
			node: 'root',
			child: [],
		};
		HTMLParser(html, {
			start: function(tag, attrs, unary) {
				debug(tag, attrs, unary);
				// node for this element
				var node = {
					node: 'element',
					tag: tag,
				};
				if (attrs.length !== 0) {
					node.attr = attrs.reduce(function(pre, attr) {
						/*console.log(attr.name);*/
						var name = attr.name;
						var value = attr.value;
						// has multi attibutes
						// make it array of attribute
						if (value.match(/ /)) {
							value = value.split(' ');
						}
						// if attr already exists
						// merge it
						if (pre[name]) {
							if (Array.isArray(pre[name])) {
								// already array, push to last
								pre[name].push(value);
							} else {
								// single value, make it array
								pre[name] = [pre[name], value];
							}
						} else {
							// not exist, put it
							pre[name] = value;
						}
						return pre;
					}, {});
				}
				if (unary) {
					// if this tag dosen't have end tag
					// like <img src="hoge.png"/>
					// add to parents
					var parent = bufArray[0] || results;
					if (parent.child === undefined) {
						parent.child = [];
					}
					parent.child.push(node);
				} else {
					bufArray.unshift(node);
				}
			},
			end: function(tag) {
				debug(tag);
				// merge into parent tag
				var node = bufArray.shift();
				if (node.tag !== tag)
					console.error('invalid state: mismatch end tag');
				if (bufArray.length === 0) {
					results.child.push(node);
				} else {
					var parent = bufArray[0];
					if (parent.child === undefined) {
						parent.child = [];
					}
					parent.child.push(node);
				}
			},
			chars: function(text) {
				debug(text);
				var node = {
					node: 'text',
					text: text,
				};
				if (bufArray.length === 0) {
					results.child.push(node);
				} else {
					var parent = bufArray[0];
					if (parent.child === undefined) {
						parent.child = [];
					}
					parent.child.push(node);
				}
			},
		});
		return results;
	};
})(this);