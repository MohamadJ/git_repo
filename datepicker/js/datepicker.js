$(document).ready(function(){
$(function() {
		    $('input[name="expiration"]').daterangepicker({
		        "singleDatePicker": true,
			    "showDropdowns": true,
			    "autoApply": true,
			    "showCustomRangeLabel": false,
			    "startDate": "01/01/2016",
			    "endDate": "12/20/2016"
			});
		});
});